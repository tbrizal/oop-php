<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new animal("shaun");

    echo "Animal Name : $sheep->name <br>";
    echo "Legs Number : $sheep->legs <br>";
    echo "Cold Blooded : $sheep->cold_blooded <br><br>";

    $sungokong = new ape("kera sakti");

    echo "Animal Name : $sungokong->name <br>";
    echo "Legs Number : $sungokong->legs <br>";
    echo "Cold Blooded : $sungokong->cold_blooded <br>";
    $sungokong->yell();
    echo "<br><br>";

    $kodok = new frog("buduk");

    echo "Animal Name : $kodok->name <br>";
    echo "Legs Number : $kodok->legs <br>";
    echo "Cold Blooded : $kodok->cold_blooded <br>";
    $kodok->jump();


?>